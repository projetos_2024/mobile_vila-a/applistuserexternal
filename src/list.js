import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
} from "react-native";
import axios from "axios";

const List = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  function userPress(user) {
    navigation.navigate("UserDetails", { user });
  }

  return (
    <View>
      <Text>Usuários!!!</Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString}
        renderItem={({ item }) => (
          <TouchableOpacity
            
            onPress={() => userPress(item)}
            >
            <Text>{item.name}</Text>
            </TouchableOpacity>
        )}
      />
    </View>
  );
};


export default List;
